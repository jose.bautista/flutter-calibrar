import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'config/beam_locations.dart';

void main() {
  runApp(ProviderScope(
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  final routeDelegate = BeamerDelegate(
      locationBuilder: SimpleLocationGenerator.simpleLocationBuilder);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: BeamerParser(),
      routerDelegate: routeDelegate,
      backButtonDispatcher: BeamerBackButtonDispatcher(delegate: routeDelegate),
      debugShowCheckedModeBanner: false,
    );
  }
}
