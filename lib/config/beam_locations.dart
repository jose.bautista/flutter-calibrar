// ignore_for_file: prefer_const_constructors

import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:reddit_clone/models/post.dart';
import 'package:reddit_clone/modules/graph/graph_screen.dart';
import 'package:reddit_clone/modules/home/screens/detail_screen.dart';
import 'package:reddit_clone/modules/home/screens/home_page.dart';

class RouteLocationGenerator {
  static const String homeRoute = '/';
  static const String graphRoute = '/graph';
  static const String detailScreenRoute = '/detailScreen';
}

class SimpleLocationGenerator {
  static final simpleLocationBuilder = RoutesLocationBuilder(routes: {
    '/': (context, state,data) => BeamPage(
          key: ValueKey('home'),
          title: 'Home',
          child: HomePage(),
          type: BeamPageType.noTransition,
        ),
    '/graph': (context, state,data) => BeamPage(
          key: ValueKey('graph'),
          title: 'Graph',
          child: GraphScreen(),
          type: BeamPageType.noTransition,
        ),
        '/graph/postId': (context, state,data) {
          final post = data as Post;
        return BeamPage(
          key: ValueKey('detailScreen'),
          title: 'Detail Screen',
          child: DetailScreen(post: post),
          type: BeamPageType.noTransition,
          popToNamed: '/'
        );
        },
  });
}
