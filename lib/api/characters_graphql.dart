import 'package:graphql/client.dart';
import 'package:reddit_clone/config/graphql.dart';

import 'package:reddit_clone/models/character.dart';

class CharacterGraphQL {
  final String _getChars = r'''
query getCharacter {
  characters{
    results{
      id
      image
      species
      name
    }
  }
}''';

  Future<List<Character>> getChars() async {
    final options = QueryOptions(document: gql(_getChars));
    final response = await graphQlClient.query(options);

    if (!response.hasException) {
      final List<Object?> charList = response.data!['characters']['results'];
      final values = charList.map(
          (character) => Character.fromJson(character as Map<String, dynamic>));

      return values.toList();
    } else {
      print(response.exception);
      return [];
    }
  }
}
