class Post {
  String id;
  String body;
  String date;
  String title;
  String mainBody;

  Post({
    required this.id,
    required this.body,
    required this.date,
    required this.title,
    required this.mainBody
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: json['id'] as String? ?? '',
      body: json['body'] as String? ?? '',
      date: json['date'] as String? ?? '',
      title: json['title'] as String? ?? '',
      mainBody: json['mainBody'] as String? ?? ''
    );
  }
}
