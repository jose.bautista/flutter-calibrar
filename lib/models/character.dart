class Character {
  String? _id;
  String? _name;
  String? _image;
  String? _species;

  Character({
    String? id,
    String? name,
    String? image,
    String? species,
  }) {
    _id = id;
    _name = name;
    _image = image;
    _species = species;
  }

  factory Character.fromJson(Map<String, dynamic> json) {
    return Character(
      id: json['id'] as String? ?? '',
      name: json['name'] as String? ?? '--',
      image: json['image'] as String? ?? '',
      species: json['species'] as String? ?? '--',
    );
  }

  String? get id => _id;
  String? get name => _name;
  String? get image => _image;
  String? get species => _species;
}
