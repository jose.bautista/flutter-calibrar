import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:reddit_clone/modules/home/custom_navbar.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reddit_clone/providers/character_provider.dart';

import '../../models/character.dart';

class GraphScreen extends HookWidget {
  const GraphScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _isLoading = useState(false);
    final _charactersProvider = useProvider(characterProvider);
    var queryData = MediaQuery.of(context);

    _loadData() async {
      _isLoading.value = true;

      try {
        await _charactersProvider.getCharacters();
      } on Exception catch (error) {
        print(error);
      }

      _isLoading.value = false;
    }

    useEffect(() {
      _loadData();

      return;
    }, []);

    return Scaffold(
        backgroundColor:const Color(0xFF041C32),
        // appBar: AppBar(title: Text('Reddit'),  automaticallyImplyLeading: false),
        bottomNavigationBar: const CustomNavBar(),
        body: Builder(
          builder: (BuildContext context) {
            return ListView.builder(
                itemCount: _charactersProvider.characters.length,
                itemBuilder: (BuildContext context, int index) {
                  final character = _charactersProvider.characters[index];

                  return Padding(
                    padding: const EdgeInsets.only(
                        left: 16, right: 16, top: 4, bottom: 4),
                    child: SizedBox(
                      width: 50,
                      height: 100,
                      
                      child: CharacterListItem(
                        character: character,
                      ),
                    ),
                  );
                });
          },
        ));
  }
}

class CharacterListItem extends StatefulWidget {
  final Character character;

  const CharacterListItem({Key? key, required this.character});

  @override
  State<CharacterListItem> createState() => _CharacterListItemState();
}

class _CharacterListItemState extends State<CharacterListItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      color: Color(0xFF064663),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
      child: Row(
        children: [
          const SizedBox(width: 25),
          CircleAvatar(
            backgroundImage: NetworkImage(widget.character.image ?? ''),
            radius: 40,
          ),
          // Container(
          //   width: 150,
          //   child: Image.network(character.image ?? '')
          // ),
          const SizedBox(width: 25),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(widget.character.name ?? '',
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  )),
              Text(widget.character.species ?? '',
                  style: GoogleFonts.poppins(
                    color: Colors.white,
                      fontSize: 12, fontWeight: FontWeight.normal)),
                      
            ],
          ),
        ],
      ),
    );
  }
}
