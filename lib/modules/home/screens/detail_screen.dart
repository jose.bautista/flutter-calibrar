import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:reddit_clone/models/post.dart';
import 'package:reddit_clone/modules/home/custom_navbar.dart';

class DetailScreen extends StatelessWidget {
  final Post post;

  const DetailScreen({Key? key, required this.post})
      : super(
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    var queryData = MediaQuery.of(context);

    return Scaffold(
      backgroundColor: Color(0xFF041C32),
      body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SizedBox(
        width: queryData.size.width,
        height: queryData.size.height,
        child: Card(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            color:Color(0xFF064663),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(post.title,
                      style: GoogleFonts.poppins(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: const Color(0xFFECB365))),
                  Text(post.body,
                      style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.normal)),
                  Text(post.mainBody,
                      style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.normal)),
                ],
              ),
            ),
        ),
      ),
          )),
      bottomNavigationBar: const CustomNavBar(),
    );
  }
}
