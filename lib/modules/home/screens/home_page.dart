import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reddit_clone/modules/home/components/add_post_dialog.dart';
import 'package:reddit_clone/providers/post_provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:beamer/beamer.dart';

import '../custom_navbar.dart';

class HomePage extends HookWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _postProvider = useProvider(PostProvider);
    final _titleTextController = useTextEditingController();
    final _bodyTextController = useTextEditingController();
    final _mainBodyTextController = useTextEditingController();
    var queryData = MediaQuery.of(context);

    return Scaffold(
      backgroundColor: const Color(0xFF041C32),
      // appBar: AppBar(title: Text('Reddit')),
      bottomNavigationBar: const CustomNavBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            for (var item in _postProvider.Posts)
              Center(
                child: SizedBox(
                  width: queryData.size.width * 0.9,
                  height: queryData.size.height * 0.2,
                  child: GestureDetector(
                    onTap: () {
                      Beamer.of(context)
                          .beamToNamed('/graph/postId', data: item);
                    },
                    onLongPress: () {
                      showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                                title: const Text('Delete?'),
                                insetPadding: EdgeInsets.zero,
                                actions: [
                                  TextButton(
                                      onPressed: () {
                                        _postProvider.deletePost(id: item.id);
                                        Navigator.pop(context);
                                      },
                                      child: const Text('Yes')),
                                  TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: const Text('No'))
                                ],
                              ),
                          barrierDismissible: true);
                    },
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      color: const Color(0xFF064663),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(item.title,
                                style: GoogleFonts.poppins(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: const Color(0xFFECB365))),
                            Text(item.body,
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal)),
                            Text(
                              item.mainBody,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.normal),
                            ),
                            Text('Date added: ${item.date}',
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 10,
                                    fontWeight: FontWeight.normal,
                                    fontStyle: FontStyle.italic)),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            //Row(children: [Text(item.body), Text(item.title), Text(item.date)]),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xFF04293A),
        onPressed: () {
          showDialog(
              context: context,
              builder: (_) => AddWidgetDialog(
                  queryData: queryData,
                  titleTextController: _titleTextController,
                  bodyTextController: _bodyTextController,
                  mainBodyTextController: _mainBodyTextController,
                  postProvider: _postProvider),
              barrierDismissible: true);
        },
        child: const Icon(
          Icons.add,
          color: Color(0xFFECB365),
        ),
      ),
    );
  }
}
