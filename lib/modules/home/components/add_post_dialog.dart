import 'package:flutter/material.dart';
import 'package:reddit_clone/modules/home/components/postInput.dart';
import 'package:reddit_clone/providers/post_provider.dart';

class AddWidgetDialog extends StatelessWidget {
  const AddWidgetDialog({
    Key? key,
    required this.queryData,
    required TextEditingController titleTextController,
    required TextEditingController bodyTextController,
    required TextEditingController mainBodyTextController,
    required PostNotifier postProvider,
  }) : _titleTextController = titleTextController, _bodyTextController = bodyTextController, _mainBodyTextController = mainBodyTextController, _postProvider = postProvider, super(key: key);

  final MediaQueryData queryData;
  final TextEditingController _titleTextController;
  final TextEditingController _bodyTextController;
  final TextEditingController _mainBodyTextController;
  final PostNotifier _postProvider;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
          title: const Text('New Post'),
          insetPadding: EdgeInsets.zero,
          content: Container(
            decoration: const BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(100.0)),
              
            ),
            height: queryData.size.height * 0.4,
            width: queryData.size.width * 0.8,
            child: Column(
              children: [
                PostInput(
                  textController: _titleTextController,
                  label: 'Title',
                  icon: Icons.title,
                  isMultiline: false,
                ),
                PostInput(
                  textController: _bodyTextController,
                  label: 'subtitle',
                  icon: Icons.home,
                  isMultiline: false,
                ),
                PostInput(
                  textController: _mainBodyTextController,
                  label: 'Body',
                  icon: Icons.document_scanner,
                  isMultiline: true,
                )
              ],
            ),
          ),
          actions: [
            TextButton(onPressed: (){Navigator.pop(context);}, child: Text('Cancel')),
            TextButton(
                onPressed: () {
                  _postProvider.addPost(
                      body: _bodyTextController.text,
                      title: _titleTextController.text,
                      mainBody: _mainBodyTextController.text);
                  _titleTextController.clear();
                  _bodyTextController.clear();
                  _mainBodyTextController.clear();
                  Navigator.pop(context);
                },
                child: Text('Add'))
          ],
        );
  }
}