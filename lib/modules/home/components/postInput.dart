import 'package:flutter/material.dart';

class PostInput extends StatelessWidget {
  final TextEditingController textController;
  final String label;
  final IconData icon;
  final bool isMultiline;

  const PostInput(
      {Key? key,
      required this.textController,
      required this.label,
      required this.icon,
      required this.isMultiline})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 300,
        child: Container(
          padding: const EdgeInsets.all(5.0),
          child: TextFormField(
            controller: textController,
            
            keyboardType: isMultiline? TextInputType.multiline : TextInputType.text,
            maxLines: isMultiline? 3 : 1,
            decoration: InputDecoration(
                icon: Icon(icon),
                label: Text(label),
                border: const OutlineInputBorder()),
          ),
        ),
      ),
    );
  }
}
