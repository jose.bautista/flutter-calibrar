import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';

class CustomNavBar extends StatefulWidget {
  const CustomNavBar({Key? key}) : super(key: key);

  @override
  State<CustomNavBar> createState() => _CustomNavBarState();
}

class _CustomNavBarState extends State<CustomNavBar> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        buildNavbarItem(context, Icons.home, 1, "/"),
        buildNavbarItem(context, Icons.person, 2, "/graph")
      ],
    );
  }

  Widget buildNavbarItem(
      BuildContext context, IconData icon, int index, String route) {
    return Container(
        height: 60,
        width: MediaQuery.of(context).size.width / 2,
        decoration: const BoxDecoration(color:Color(0xFF04293A)),
        child: IconButton(
          icon: Icon(icon, color: const Color(0xFFECB365),),
          onPressed: () {
            Beamer.of(context).beamToNamed(
              route,
            );
          },
        ));
  }
}
