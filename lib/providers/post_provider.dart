import 'package:flutter/cupertino.dart';
import 'package:reddit_clone/models/post.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final PostProvider = ChangeNotifierProvider<PostNotifier>((ref) {
  return PostNotifier();
});

class PostNotifier extends ChangeNotifier {
  List<Post> _Posts = [
    Post(
        id: '1',
        title: 'Squirtle',
        body: 'Tiny Turtle Pokemon',
        mainBody:
            "Squirtle physically debuted in Here Comes the Squirtle Squad as the leader of the Squirtle Squad, a gang of rogue Squirtle who were deserted by their Trainers. Ash befriended and caught this Squirtle, who stayed with Ash until The Fire-ring Squad!, when it returned to the Squirtle Squad. However, it briefly reappeared in Pokémon the Series: Ruby and Sapphire, to assist Ash with his final battle against Brandon.",
        date: DateTime.now().toString()),
    Post(
        id: '2',
        body: 'Fire Pokemon',
        title: 'Charmander',
        mainBody:
            "Charmander physically debuted in Charmander - The Stray Pokémon, where Ash, Misty, and Brock found it after it was abandoned by its Trainer, Damian, who only cared about raising strong Pokémon. At first, Charmander refused to leave the spot it was at, believing Damian would return for it. However, when a rainstorm picked up, Charmander grew ill and Ash took it to a Pokémon Center. Charmander realized that Damian was no good, and allowed Ash to catch it. It has since evolved into Charmeleon, and then Charizard.",
        date: DateTime.now().toString()),
    Post(
        id: '3',
        body: 'Grass Pokemon',
        mainBody:
            "It is found in grasslands and forests throughout the Kanto region. However, due to Bulbasaur's status as starter Pokémon, it is hard to come by in the wild and generally found under the ownership of a Trainer. It has been observed that a Bulbasaur's bulb will flash blue when it is ready to evolve. If it does not want to evolve, it struggles to resist the transformation. Many Bulbasaur gather every year in a hidden garden in Kanto to evolve into Ivysaur in a ceremony led by a Venusaur.",
        title: 'Bulbasaur',
        date: DateTime.now().toString()),
    Post(
        id: '4',
        body: 'Normal Pokemon',
        mainBody:
            'Eevee evolves into Glaceon when leveled up near an Ice Rock (Generations IV to VII and Brilliant Diamond and Shining Pearl), when exposed to an Ice Stone (Sword and Shield), or either through exposure to an Ice Stone or the evolution being forced in proximity to an Ice Rock (Legends: Arceus).',
        title: 'Eevee',
        date: DateTime.now().toString())
  ];
  List<Post> get Posts => _Posts;

  Future<List<Post>> getPosts() async {
    final data = await getPosts();
    _Posts = data;
    return [
      Post(
          id: '1',
          title: 'Squirtle',
          body: 'Tiny Turtle Pokemon',
          mainBody:
              "Squirtle physically debuted in Here Comes the Squirtle Squad as the leader of the Squirtle Squad, a gang of rogue Squirtle who were deserted by their Trainers. Ash befriended and caught this Squirtle, who stayed with Ash until The Fire-ring Squad!, when it returned to the Squirtle Squad. However, it briefly reappeared in Pokémon the Series: Ruby and Sapphire, to assist Ash with his final battle against Brandon.",
          date: DateTime.now().toString()),
      Post(
          id: '2',
          body: 'Fire Pokemon',
          title: 'Charmander',
          mainBody:
              "Charmander physically debuted in Charmander - The Stray Pokémon, where Ash, Misty, and Brock found it after it was abandoned by its Trainer, Damian, who only cared about raising strong Pokémon. At first, Charmander refused to leave the spot it was at, believing Damian would return for it. However, when a rainstorm picked up, Charmander grew ill and Ash took it to a Pokémon Center. Charmander realized that Damian was no good, and allowed Ash to catch it. It has since evolved into Charmeleon, and then Charizard.",
          date: DateTime.now().toString()),
      Post(
          id: '3',
          body: 'Grass Pokemon',
          title: 'Bulbasaur',
          mainBody:
              "It is found in grasslands and forests throughout the Kanto region. However, due to Bulbasaur's status as starter Pokémon, it is hard to come by in the wild and generally found under the ownership of a Trainer. It has been observed that a Bulbasaur's bulb will flash blue when it is ready to evolve. If it does not want to evolve, it struggles to resist the transformation. Many Bulbasaur gather every year in a hidden garden in Kanto to evolve into Ivysaur in a ceremony led by a Venusaur.",
          date: DateTime.now().toString()),
      Post(
          id: '4',
          body: 'Normal Pokemon',
          mainBody:
              'Eevee evolves into Glaceon when leveled up near an Ice Rock (Generations IV to VII and Brilliant Diamond and Shining Pearl), when exposed to an Ice Stone (Sword and Shield), or either through exposure to an Ice Stone or the evolution being forced in proximity to an Ice Rock (Legends: Arceus).',
          title: 'Eevee',
          date: DateTime.now().toString())
    ];
  }


  Future<void> addPost(
      {required String body,
      required String title,
      required String mainBody}) async {
    final newPost = Post(
      id: (_Posts.length + 1).toString(), //uuid
      body: body,
      date: DateTime.now().toString(), title: title,
      mainBody: mainBody,
    );

    _Posts = [..._Posts, newPost];
    notifyListeners();
  }

  Future<void> deletePost({required String id}) async {
    _Posts.removeWhere((Post) => Post.id == id);
    notifyListeners();
  }
}
