import 'package:flutter/cupertino.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:reddit_clone/api/characters_graphql.dart';
import 'package:reddit_clone/models/character.dart';
import 'package:reddit_clone/providers/api_provider.dart';

final characterProvider = ChangeNotifierProvider(
  (ref) {
    final charactersGraphQL = ref.watch(apiProvider).characters;

    return CharacterNotifier(charactersAPI: charactersGraphQL);
  },
);

class CharacterNotifier extends ChangeNotifier {
  final CharacterGraphQL charactersAPI;

  CharacterNotifier({
    required this.charactersAPI,
  });

  List<Character> _characters = [];
  List<Character> get characters => _characters; //ask bruce about this

  Future<List<Character>> getCharacters() async {
    final charList = await charactersAPI.getChars();

    _characters = charList;
    return _characters;
  }
}
